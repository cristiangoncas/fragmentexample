package cristian.com.fragmentexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    FragmentController fc;
    int container1, container2;
    Fragment1 f1;
    Fragment2 f2;
    Fragment3 f3;
    Fragment4 f4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fc = FragmentController.getInstance(getSupportFragmentManager());
        container1 = R.id.fragment_container_1;
        container2 = R.id.fragment_container_2;

        f1 = new Fragment1();
        f2 = new Fragment2();
        f3 = new Fragment3();
        f4 = new Fragment4();

        Button btn11 = (Button) findViewById(R.id.btn11);
        Button btn12 = (Button) findViewById(R.id.btn12);
        Button btn13 = (Button) findViewById(R.id.btn13);
        Button btn14 = (Button) findViewById(R.id.btn14);
        Button btn21 = (Button) findViewById(R.id.btn21);
        Button btn22 = (Button) findViewById(R.id.btn22);
        Button btn23 = (Button) findViewById(R.id.btn23);
        Button btn24 = (Button) findViewById(R.id.btn24);

        btn11.setOnClickListener(this);
        btn12.setOnClickListener(this);
        btn13.setOnClickListener(this);
        btn14.setOnClickListener(this);
        btn21.setOnClickListener(this);
        btn22.setOnClickListener(this);
        btn23.setOnClickListener(this);
        btn24.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn11:
                fc.showFragment(container1, ((Fragment1) getClass()), null);
                break;
            case R.id.btn12:
                fc.showFragment(container1, f2, null);
                break;
            case R.id.btn13:
                fc.showFragment(container1, f3, null);
                break;
            case R.id.btn14:
                fc.showFragment(container1, f4, null);
                break;
            case R.id.btn21:
                fc.showFragment(container2, f1, null);
                break;
            case R.id.btn22:
                fc.showFragment(container2, f2, null);
                break;
            case R.id.btn23:
                fc.showFragment(container2, f3, null);
                break;
            case R.id.btn24:
                fc.showFragment(container2, f4, null);
                break;
        }
    }
}
