package cristian.com.fragmentexample;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;

public class FragmentController {

    private static FragmentController instance = null;
    private Fragment currentFragment = null;

    private FragmentManager mFragmentManager;

    public synchronized static FragmentController getInstance(FragmentManager fragmentManager) {
        if (instance == null) {
            instance = new FragmentController(fragmentManager);
        }
        return instance;
    }

    public FragmentController(FragmentManager fragmentManager) {
        mFragmentManager = fragmentManager;
    }

    public void showFragment(int container, Fragment fragment, @Nullable String label) {
        if (mFragmentManager.findFragmentById(fragment.getId()) == null) {
            addFragment(container, fragment, label);
        } else {
            changeFragment(container, fragment, label);
        }
    }

    public void hideFragment(Fragment fragment) {
        removeFragment(fragment);
    }

    private void addFragment(int container, Fragment fragment, @Nullable String label) {
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.add(container, fragment, label);
        commitFragment(transaction);
    }

    private void changeFragment(int container, Fragment fragment, @Nullable String label) {
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.replace(container, fragment, label);
        commitFragment(transaction);
    }

    private void removeFragment(Fragment fragment) {
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.remove(fragment);
        commitFragment(transaction);
    }

    private void addFragmentToBackstack() {

    }

    private void commitFragment(FragmentTransaction transaction) {
        transaction.commit();
    }

}